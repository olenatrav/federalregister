package framework;

import com.codeborne.selenide.WebDriverRunner;
import org.apache.commons.io.FileUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;

import java.awt.*;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.codeborne.selenide.Condition.not;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selectors.byXpath;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.sleep;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;
import static framework.Constants.CONSTANT_10_SECONDS;


/**
 * Created by User on 26.08.2016.
 */
public class WebDriverCommands
{

    public static String errorMessage = "";

    public static WebDriver driver;


    public boolean isAlertPresents()
    {
        try
        {
            driver.switchTo().alert();
            return true;
        }
        catch (Exception e)
        {
            return false;
        }
    }

    public void closeLotteryPopUp() throws Exception
    {
        URL obj = new URL("http://gateway.travellata.ru/apiV1/lottery/findActive");
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        BufferedReader input = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine = "";
        StringBuffer response = new StringBuffer();
        while ((inputLine = input.readLine()) != null)
        {
            response.append(inputLine);
        }
        input.close();

        if(json(response.toString(), "data") != null)
        {
            String data = json(response.toString(), "data");
            String id = json(data, "id");

            Cookie cookie = new Cookie("lottery_" + id + "_closed", "1");
            getWebDriver().manage().addCookie(cookie);
            getWebDriver().navigate().refresh();
        }




    }



    public static void shutDown(Throwable t)
    {

        errorMessage = errorMessage + "\n" + t.getMessage();
        System.out.println(errorMessage);


        getWebDriver().manage().deleteAllCookies();
        getWebDriver().quit();
        System.exit(1);
    }

    public static void editErrorMessage(String message)
    {
        errorMessage = errorMessage + "\n" + message;
    }


    public static void checkSuccessLoad()
    {
        Assert.assertTrue($(byXpath("//div[@class = 'content-404']")).is(not(visible)));
    }

    public static void clickAndWait(String xpath)
    {
        try
        {
            $(byXpath(xpath)).waitUntil(visible, CONSTANT_10_SECONDS).click();
        }
        catch (TimeoutException e)
        {
            $(byXpath("//body")).pressEscape();
        }
    }

    public static void goToPage(String url)
    {
        try
        {
            driver.navigate().to(url);
        }
        catch (TimeoutException te)
        {
            $(byXpath("//body")).pressEscape();
        }
        catch (Throwable t)
        {
            shutDown(t);
        }

        editErrorMessage("go to page:" + url);

    }

    public static void goToPageWithClearCache(String url)
    {
        try
        {
            driver.manage().deleteAllCookies();
            driver.navigate().refresh();
            driver.navigate().to(url);
        }
        catch (TimeoutException te)
        {
            $(byXpath("//body")).pressEscape();
        }
        catch (Throwable t)
        {
            shutDown(t);
        }

        editErrorMessage("go to page:" + url);

    }

    public static void takeScreenShot(String screenShotFilePath) throws IOException
    {
            File scrFile = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
            try{
                FileUtils.copyFile(scrFile, new File(screenShotFilePath));
            }

            catch (IOException e)
            {
                System.out.println(e.getMessage());

            }
    }

    public static void scrollUpOnPage() throws Exception{
        driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.HOME);
    }

    public static void scrollDownOnPage() throws Exception{
        driver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.END);
    }

    public static String getCurrentUrl() throws Exception{

        return driver.getCurrentUrl();
    }

    public String json(String response, String node) throws Exception
    {
        JSONParser jsonParser = new JSONParser();

        Object obj = jsonParser.parse(response);

        JSONObject jsonObjects = (JSONObject) obj;

        String s = jsonObjects.get(node).toString();

        return s;
    }




    public String fromJsonpToJsonParse(String JsonpResponse)
    {
        String json = JsonpResponse.replaceFirst(".*?\\(", "").replaceFirst("\\);", "");

        return json;
    }

    public String inventoryErrorSource(String inventoryUrl)
    {
        Matcher matcher;
        Pattern errorSourcePattern = Pattern.compile("inventory\\w*");
        matcher = errorSourcePattern.matcher(inventoryUrl);
        matcher.find();

        return matcher.group(0);
    }

    public String getKeyFromValue(Map hm, Object value)
    {
        Set ref = hm.keySet();
        Iterator iterator = ref.iterator();
        String key = new String();

        while (iterator.hasNext())
        {
            Object o = iterator.next();
            if (hm.get(o).equals(value))
            {
                key = o.toString();
            }
        }
        return key;
    }


    /**
     * Method waits for timeout interval
     * @param secsToWait   Integer Interval in sec to wait
     */
    public void waitTime(int secsToWait) {

        try {
            for (int second = 0; ; second++) {
                if ((second > secsToWait)) {
                    break;
                }
                Thread.sleep(1000);
            }
        } catch (Exception e) {
            e.fillInStackTrace();
        }

    }

    /**
     * Method switch to new browser tab
     */
    public static void switchToNewTab(int tabNumber){ //tab number starts from 0

        ArrayList<String> newTab = new ArrayList<String>(WebDriverRunner.getWebDriver().getWindowHandles());
//        newTab.remove(oldTab);
        // change focus to new tab
        WebDriverRunner.getWebDriver().switchTo().window(newTab.get(tabNumber));
    }

    public void closeOldTab()
    {
        ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tab.get(0));
        driver.close();
        tab.remove(tab.size());
    }

    public void closeTab(int tabNumber)
    {
        ArrayList<String> tab = new ArrayList<String>(driver.getWindowHandles());
        driver.switchTo().window(tab.get(tabNumber));
        driver.close();
        tab.remove(tab.size());
    }

    /**
     * Method clear element
     * @param by By Element
     */
    public void clear(By by){
        if (isElementPresent(by)){
            driver.findElement(by).clear();
        }
    }

    /**
     * Method send keys to element
     * @param by    By Element
     * @param value String value
     */
    public void sendKeys(By by, String value){
        if (isElementPresent(by)){
            driver.findElement(by).sendKeys(value);
        }
    }

    public static String  getFullDateFromCurrent(int daysCount){
        Calendar c = Calendar.getInstance();
        Date dt = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        c.setTime(dt);
        c.add(Calendar.DATE, daysCount);

        return dateFormat.format(c.getTime());
    }
    public static String getFullDateFrom(String startDate, int nights) throws Exception
    {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy");
        Date date = formatter.parse(startDate);
        c.setTime(date);
        c.add(Calendar.DATE, nights);

        return formatter.format(c.getTime());
    }

    public static String getDateFrom(String startDate, int nights) throws Exception
    {
        Calendar c = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM");
        Date date = formatter.parse(startDate);
        c.setTime(date);
        c.add(Calendar.DATE, nights);

        return formatter.format(c.getTime());
    }

    public String deleteFirstZeroFromDate(String dayValue){
        dayValue = dayValue.replaceFirst("^0", "");
        return dayValue;
    }

    final private String EMPTY_RESULT_BLOCK = "//.[contains(text(), 'Пока не найдено ни одной записи')]";
    final private String PAGINATION_BLOCK = "//div[@class = 'pagination-container']/ul";
    final private String PAGINATION_TOTAL_PAGES_BUTTONS = "//ul[contains(@ng-show, 'totalPages')]/li";
    final private String PAGINATION_LAST_PAGE_BUTTON = "//ul[contains(@ng-show, 'totalPages')]/li[last()-1]/a";
    final private String PAGINATION_NEXT_BUTTON = "//a[text() = 'Next']";
    final private String PAGINATION_FIRST_PAGE_BUTTON = "//a[text() = '1']";
    final private String PAGINATION_LAST_POSSIBLE_PAGE_BUTTON = "//.[text() = 'Next']/../preceding-sibling::li[1]/a";
    final private String ORDERS_LIST = "//tbody/tr";

    final private String FILTER_ORDER_START_DATE = "//tbody/tr[%s]/td[6]";
    final private String FILTER_ORDER_TOUR_OPERATOR = "//tbody/tr[%s]/td[5]";
    final private String FILTER_ORDER_COUNTRY = "//tbody/tr[%s]/td[12]";
    final private String FILTER_ORDER_RESORT = "//tbody/tr[%s]/td[13]";
    final private String FILTER_ORDER_SHOWCASE = "//tbody/tr[%s]/td[10]";
    final private String FILTER_ORDER_SELLER = "//tbody/tr[%s]/td[4]";



    final private String FILTER_ORDER_STATUS = "//tbody/tr[%s]/td[21]";
    final private String FILTER_ORDER_FIRST_PAYMENT = "//tbody/tr[%s]/td[26]";
    final private String FILTER_ORDER_LAST_PAYMENT = "//tbody/tr[%s]/td[27]";
    final private String FILTER_ORDER_SOLD_DATE = "//tbody/tr[%s]/td[8]";
    final private String FILTER_ORDER_VISA = "//tbody/tr[1]/td[14]/span[1]";
    final private String FILTER_ORDER_VISA_SUPPORT = "//tbody/tr[1]/td[14]/span[2]";
    final private String FILTER_ORDER_CUSTOMER_PAYED = "//tbody/tr[%s]/td[10]";
    final private String FILTER_ORDER_PAYED_TO_TO = "//tbody/tr[%s]/td[12]/a";


    public void checkPosition(String xpath, String position)
    {
        String[] location = position.split(", ");

        Assert.assertTrue($(byXpath(xpath)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getX() == Integer.parseInt(location[0]));
        Assert.assertTrue($(byXpath(xpath)).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getY() == Integer.parseInt(location[1]));

    }

    public void getPositionOfElementsOnPage(String[] xpaths)
    {
        for(int i = 0; i < xpaths.length; i++)
        {
            System.out.println("private final String _POSITION = \"" + $(byXpath(xpaths[i])).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getX() +
                    ", " +
                    $(byXpath(xpaths[i])).waitUntil(visible, CONSTANT_10_SECONDS).getLocation().getY() + "\";");
        }
    }


    /**
     * Click some element
     * @param by    By  Element that is needed to be clicked
     */
    public void click(By by)
    {
        if (isElementDisplayed(by))
        {
            driver.findElement(by).click();
        }

    }

    /**
     * Check if element is displayed on page
     * @param by    By  Element that is needed to be checked
     * @return  true:   if element is displayed
     *          false:  otherwise
     */
    public boolean isElementDisplayed(By by)
    {
        return driver.findElement(by).isDisplayed();
    }

    /**
     * Wait for element not to be visible
     * @param by        By  Needed element to be waited for
     * @param timeOut   Integer Time limit for element not to be visible
     */
    public void waitForElementNotVisible(final By by, int timeOut)
    {
        (new WebDriverWait(driver, timeOut))
                .until(ExpectedConditions
                        .invisibilityOfElementLocated(by));
    }

    /**
     * Check if element is present
     * @param by    By  Needed element to be checked
     * @return  true:   element is present
     *          false:  otherwise
     */
    public boolean isElementPresent(By by)
    {
        try
        {
            driver.findElement(by);
            return true;
        } catch (org.openqa.selenium.NoSuchElementException e)
        {
            return false;
        }
    }

    /*public void waitForElementPresent(By by, int timeOut)
    {
        for (int i=0; i<timeOut; i++){
            if (isElementPresent(by)) break;
        }
    }*/

    /**
     * Wait for element to be displayed
     * @param by        By  Needed element to be waited for
     * @param timeOut   Integer Time limit for element to be displayed
     */
    public void waitForElementDisplayed(final By by, int timeOut)
    {
        try
        {
            (new WebDriverWait(driver, timeOut)).until(ExpectedConditions.visibilityOfElementLocated(by));
        }
        catch (TimeoutException timeout)
        {
            String currentUrl = driver.getCurrentUrl();
            System.out.println("Error source: " + currentUrl);
        }

    }


    /**
     * Find element on page
     * @param by    By  Needed element to be found
     * @return  WebElement object
     */
    public WebElement findElement (By by)
    {
        return driver.findElement(by);
    }

    public static String getCurrentDate(){
        Date dateNow = new Date();
        SimpleDateFormat formatForDateNow = new SimpleDateFormat("dd.MM.yyyy");


         return formatForDateNow.format(dateNow);
    }

    public String[] getUSDRate(boolean multiplier) throws Exception
    {
        URL obj = new URL("https://www.cbr-xml-daily.ru/daily_json.js");
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        BufferedReader input = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine = "";
        StringBuffer response = new StringBuffer();
        while ((inputLine = input.readLine()) != null)
        {
            response.append(inputLine);
        }
        input.close();

        String result = json(response.toString(), "Valute");
        result = json(result.toString(), "USD");
        String previousValue = json(result.toString(), "Previous");
        String currentValue = json(result.toString(), "Value");
        String[] values = new String[2];
        values[0] = previousValue;
        values[1] = currentValue;

        //GATE-1279
        if(multiplier)
        {
            values[0] = Double.toString(Double.parseDouble(previousValue) * 1.025);
            values[1] = Double.toString(Double.parseDouble(currentValue) * 1.025);
        }

        return values;
    }

    public String[] getEuroRate(boolean multiplier) throws Exception
    {
        URL obj = new URL("https://www.cbr-xml-daily.ru/daily_json.js");
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        BufferedReader input = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine = "";
        StringBuffer response = new StringBuffer();
        while ((inputLine = input.readLine()) != null)
        {
            response.append(inputLine);
        }
        input.close();

        String result = json(response.toString(), "Valute");
        result = json(result.toString(), "EUR");
        String previousValue = json(result.toString(), "Previous");
        String currentValue = json(result.toString(), "Value");
        String[] values = new String[2];
        values[0] = previousValue;
        values[1] = currentValue;

        //GATE-1279
        if(multiplier)
        {
            values[0] = Double.toString(Double.parseDouble(previousValue) * 1.025);
            values[1] = Double.toString(Double.parseDouble(currentValue) * 1.025);
        }

        return values;
    }

    public String[] getVisaInfo(String countryID) throws Exception
    {
        URL obj = new URL("http://content.travellata.lan/apiV2/country/details?countryId=" + countryID);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        BufferedReader input = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine = "";
        StringBuffer response = new StringBuffer();
        while ((inputLine = input.readLine()) != null)
        {
            response.append(inputLine);
        }
        input.close();

        String result = json(response.toString(), "data");

        String visaRequired = json(result.toString(), "visaRequired");
        String currencyExchange = json(result.toString(), "currencyExchange");
        String visaPaymentAdult = json(result.toString(), "visaPayment");
        String visaInfantValue = json(result.toString(), "visaPaymentChildLessSix");
        String visaKidValue = json(result.toString(), "visaPaymentChildFromSixToTwelve");

        String[] values = new String[5];
        values[0] = visaRequired;
        values[1] = currencyExchange;
        values[2] = visaPaymentAdult;
        values[3] = visaInfantValue;
        values[4] = visaKidValue;

        return values;
    }






    public boolean compareValuesWithError(String firstValue, String secondValue, int error)
    {
        boolean success = false;

        int f = Integer.parseInt(firstValue);
        int s = Integer.parseInt(secondValue);
        int sum = 0;

        if (f > s)
        {
            sum = f - s;
        }
        else
        if(s > f)
        {
            sum = s - f;
        }

        if(sum < error)
        {
            success = true;
        }


        return success;
    }

    public String getCountryCurrency(String countryID) throws Exception
    {
        URL obj = new URL("http://content.travellata.lan/apiV2/country/details?countryId=" + countryID);
        HttpURLConnection con = (HttpURLConnection) obj.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        BufferedReader input = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine = "";
        StringBuffer response = new StringBuffer();
        while ((inputLine = input.readLine()) != null)
        {
            response.append(inputLine);
        }
        input.close();

        String result = json(response.toString(), "data");

        String currency = json(result.toString(), "currency");

        return currency;
    }

    public static void refreshPage(){
        driver.manage().deleteAllCookies();
        driver.navigate().refresh();
    }

    public void retryClickByXpath(String element) {
        int attempts = 0;
        while(attempts < 2) {
            try {
                $(byXpath(element)).click();
                break;
            } catch(Exception e) {
            }
            attempts++;
        }
    }

    public void setFullScreenMode(){
        try {
            Robot robot = new Robot();
            sleep(2000);
            robot.keyPress(KeyEvent.VK_F11);
        } catch (AWTException e) {
            System.err.println("Exception (ignored) " + e.toString());
        }
    }

    public void moveToElement(WebElement element){
        Actions actions = new Actions(driver);
        actions.moveToElement(element).build().perform();
    }

}

package framework;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

import static framework.Constants.mailFrom;
import static framework.Constants.mailHost;

public class Sender {

    public static void sendMail(String toEmail, String attachFileName, String subjectValue, String countryValue){

        //Get the session object
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", mailHost);
//        properties.setProperty("mail.smtp.port", mailPort);
        Session session = Session.getDefaultInstance(properties);

        //compose the message
        try{
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(mailFrom));
            ArrayList<String> recipientsList = new ArrayList<String>(Arrays.asList(toEmail.split("\\s*,\\s*")));
            for(int i=0; i<recipientsList.size(); i++)
                message.addRecipient(Message.RecipientType.TO,new InternetAddress(recipientsList.get(i)));
            String currentDate = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
            message.setSubject(subjectValue + " " + currentDate);

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Now set the actual message
            messageBodyPart.setText("Doc is in attachment. Have a good day!");

            // Create a multipart message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment
            messageBodyPart = new MimeBodyPart();
            DataSource source = new FileDataSource(attachFileName);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(attachFileName);
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            message.setContent(multipart);
            // Send message
            Transport.send(message);
            System.out.println( countryValue + " report sent successfully....");

        }catch (MessagingException mex) {mex.printStackTrace();}
    }

    public static void sendHotelsListMail(String toEmail, String attachFileName, String attachDeltaFileName, String subjectValue){

        //Get the session object
        Properties properties = System.getProperties();
        properties.setProperty("mail.smtp.host", mailHost);
//        properties.setProperty("mail.smtp.port", mailPort);
        Session session = Session.getDefaultInstance(properties);

        //compose the message
        try{
            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(mailFrom));
            ArrayList<String> recipientsList = new ArrayList<String>(Arrays.asList(toEmail.split("\\s*,\\s*")));
            for(int i=0; i<recipientsList.size(); i++)
                message.addRecipient(Message.RecipientType.TO,new InternetAddress(recipientsList.get(i)));
            String currentDate = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
            message.setSubject(subjectValue + " " + currentDate);

            // Create the message part
            BodyPart messageBodyPart = new MimeBodyPart();

            // Now set the actual message
            messageBodyPart.setText("Doc is in attachment. Have a good day!");

            // Create a multipart message
            Multipart multipart = new MimeMultipart();

            // Set text message part
            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment
            messageBodyPart = new MimeBodyPart();
            DataSource source = new FileDataSource(attachFileName);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(attachFileName);
            multipart.addBodyPart(messageBodyPart);

            // Part three is another attachment
            messageBodyPart = new MimeBodyPart();
            source = new FileDataSource(attachDeltaFileName);
            messageBodyPart.setDataHandler(new DataHandler(source));
            messageBodyPart.setFileName(attachDeltaFileName);
            multipart.addBodyPart(messageBodyPart);

            // Send the complete message parts
            message.setContent(multipart);
            // Send message
            Transport.send(message);
            System.out.println( "Report sent successfully....");

        }catch (MessagingException mex) {mex.printStackTrace();}
    }

}

package framework;

import com.codeborne.selenide.WebDriverRunner;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import static com.codeborne.selenide.WebDriverRunner.getWebDriver;


/**
 * Created by User on 26.08.2016.
 */
public class SeleniumTestCase extends WebDriverCommands {

    @BeforeClass
    public static void setUpClass() {

//        WebDriverManager.chromedriver().setup();
        WebDriverManager.firefoxdriver().setup();

    }



    @BeforeMethod
    public void setUp() throws Exception
    {

//        ChromeOptions options = new ChromeOptions();

        //for CI server
//        options.addArguments("--disable-gpu");
//        options.addArguments("--no-sandbox"); //for CI server
//        options.addArguments("--headless"); //for CI server
//        options.addArguments("--window-size=1980,1080"); //for CI server

        //for local machine
//        options.addArguments("--start-maximized");
//        options.addArguments("--hide-scrollbars");
//        options.addArguments("--disable-popup-blocking");
//        options.addArguments("--disable-notifications");
//        options.addArguments("--disable-extensions");
//
//        DesiredCapabilities capabilities = DesiredCapabilities.chrome();
//        capabilities.setCapability(ChromeOptions.CAPABILITY, options);
//        capabilities.setCapability("pageLoadStrategy", "normal");
//        capabilities.setCapability(CapabilityType.UNEXPECTED_ALERT_BEHAVIOUR, UnexpectedAlertBehaviour.IGNORE);
//
//        driver = new ChromeDriver(options);
        driver = new FirefoxDriver();

        WebDriverRunner.setWebDriver(driver);
        if (driver instanceof JavascriptExecutor) {
            ((JavascriptExecutor)driver).executeScript("Object.defineProperty(navigator, 'webdriver', {get: () => undefined})");
        } else {
            throw new IllegalStateException("This driver does not support JavaScript!");
        }

    }


    @AfterMethod
    public void tearDown()
    {
        System.out.println(errorMessage);
        getWebDriver().manage().deleteAllCookies();
        getWebDriver().quit();
    }

}